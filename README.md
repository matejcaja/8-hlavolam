#Zadanie

Cieľom tohto zadania je nájsť riešenie pre 8-hlavolam. Jedná sa o hlavolam zloženy z 8 označených políčok (najčastejšie číslom alebo písmenom abecedy) a jedného prázdneho miesta. Označené políčka je možné presúvať 4 smermi, konkrétne Hore,Dole,Vľavo, Vpravo, ale iba v prípade, ak sa v danom smere pohybu nachádza prázdne miesto a pochybom sa neopustí hracia plocha. Na riešenie problému je vždy na začiatku daná začiatočná a koncova pozícia, pričom cieľom je nájsť postupnosť krokov, ktoré vedú od začiatočnej pozície do cieľovej.

Na vyriešenie danej úlohy bol použitý algoritmus A* s využitím dvoch heuristík a ich následného porovnania.

Pre podrobnú dokumentáciu viď. súbor dokumentácia.pdf
