public class Ramec {
    public int g; //cena cesty do uzla konstanta +1
    public int h; //cena cesty z uzla do ciela
    public int f; //f = g+h
    public int[][] uzly = new int[3][3]; //pozicie policok pre dany Ramec
    public String predchodca; //odkaz na predchodcu
    private String id; //jedinecny identifikator, vytvoreny ako String z pozicii policok daneho Ramca
    public int smery; //smer


    public Ramec(){

    }

    public Ramec(int[][] input){
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                uzly[i][j] = input[i][j];
            }
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



}
