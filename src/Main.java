import javax.swing.*;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class Main {
    public void Astar(JTextArea textArea, int value, int heuristika){
        Map<String, Ramec> openList = new HashMap<String, Ramec>();
        Map<String, Ramec> closedList = new HashMap<String, Ramec>();
        PriorityQueue<Ramec> priorityQueue = new PriorityQueue(20, new RamecManager());
        RamecManager manager = new RamecManager();
        XmlManager xml = new XmlManager();
        double startTime;
        double endTime;
        String number = value+"";

        manager.vynulujSMery(RamecManager.smery);

        Ramec startRamec = new Ramec(xml.xmlRead("start_"+number));
        Ramec resultRamec = new Ramec(xml.xmlRead("result_"+number));

        //nastavenie zaciatocnych hodnot
        startRamec.predchodca = "0";
        startRamec.uzly = manager.copyCurrentUzly(xml.xmlRead("start_"+number));
        startRamec.g = 0;
        startRamec.h = manager.findMyPlace(startRamec.uzly, resultRamec.uzly, heuristika);
        startRamec.f = startRamec.h + startRamec.g;
        startRamec.setId(manager.saveArrayToString(startRamec.uzly));
        resultRamec.setId(manager.saveArrayToString(resultRamec.uzly));

        priorityQueue.add(startRamec);
        openList.put(startRamec.getId(), startRamec);


        Ramec minRamec = null;

        startTime = System.currentTimeMillis();
        while(!openList.isEmpty()){
            //ziskaj ramec s najmensim f ohodnotenim
            minRamec = priorityQueue.poll();
            //vyber ramec z listu
            openList.remove(minRamec.getId());

            //ak je to ciel, tak skonci
            if(minRamec.getId().equals(resultRamec.getId())){
                closedList.put(minRamec.getId(), minRamec);
                break;
            }else{
            //inac vloz ramec do closedList-u
                closedList.put(minRamec.getId(), minRamec);

                //ziskaj vsetkych jeho nasledovnikov
                while(true){
                    //zisti kam sa mozes pohnut
                    int[][] newUzly = manager.calculateStep(minRamec.uzly);
                    //ak uz nemas kam ist, skonci
                    if(newUzly == null)
                        break;

                    //ak uz sa raz spracovaval, chod odznova na dalsieho nasledovnika
                    if(closedList.get(manager.saveArrayToString(newUzly)) != null){
                        continue;
                    //ak nie, vytvor novy ramec, nastav mu hodnoty a vloz ho do openList-u
                    }else if(openList.get(manager.saveArrayToString(newUzly)) == null){
                        Ramec newRamec = new Ramec();
                        newRamec.smery = manager.findDirection();
                        newRamec.uzly = manager.copyCurrentUzly(newUzly);
                        newRamec.predchodca = minRamec.getId();
                        newRamec.setId(manager.saveArrayToString(newRamec.uzly));
                        newRamec.g = minRamec.g+1;
                        newRamec.h = manager.findMyPlace(newRamec.uzly, resultRamec.uzly, heuristika);
                        newRamec.f = newRamec.g + newRamec.h;

                        priorityQueue.add(newRamec);
                        openList.put(newRamec.getId(), newRamec);
                    }
                }
                //ked najdem vsetkych nasledovnikov, vynulujem si smery
                manager.vynulujSMery(RamecManager.smery);
            }
        }


        StringBuffer cesta = new StringBuffer();
        while(!minRamec.predchodca.equals("0") && minRamec != null){
                for(int i = 0; i < 3; i++){
                    for(int j = 0; j < 3; j++){
                        textArea.append((minRamec.uzly[i][j]) + " ");

                    }
                    textArea.append("\n");

                }
                cesta.append(manager.translateDirection(minRamec.smery) +";");
                textArea.append("Hlbka uzla: "+minRamec.g+"\n"+"Smer: "+manager.translateDirection(minRamec.smery)+"\nHeuristika: "+minRamec.h  +" \n\n");

                minRamec = closedList.get(minRamec.predchodca);

        }
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
               textArea.append((minRamec.uzly[i][j]) + " ");

            }
            textArea.append("\n");

        }
        endTime=System.currentTimeMillis();
        textArea.append("Hlbka uzla: "+minRamec.g+"\n\n");
        textArea.append("Pocet nespracovanych uzlov: "+openList.size() +"\nPocet spracovanych uzlov: "+closedList.size()+"\nCas vypoctu: "+((endTime-startTime)/1000)+"s\n");
        textArea.append(cesta+"\n");
        System.out.println(((endTime-startTime)/1000)+";");
    }
}
