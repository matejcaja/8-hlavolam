import javax.swing.*;
import java.awt.event.*;
import java.io.File;

public class GuiDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextArea textArea1;
    private JTextArea textArea2;
    private JComboBox comboBox1;
    private JRadioButton radioButton1;
    private JRadioButton radioButton2;
    private JButton clearButton;
    private JButton openButton;
    private final JFileChooser chooser;
    ButtonGroup bG = new ButtonGroup();
    Main main = new Main();
    XmlManager xml;
    File file;

    public GuiDialog() {
        setTitle("A* algoritmus (c) Matej Čaja");
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        buttonOK.setEnabled(false);


        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                    onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);


        bG.add(radioButton1);
        bG.add(radioButton2);
        bG.setSelected(radioButton1.getModel(),true);
        chooser = new JFileChooser();

        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                textArea1.setText("");
            }
        });
        openButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                textArea2.setText("");
                int value = chooser.showOpenDialog(contentPane);

                if(value == JFileChooser.APPROVE_OPTION){
                    file = chooser.getSelectedFile();
                    xml = new XmlManager(file);
                    xml.xmlShow(textArea2,comboBox1);
                    buttonOK.setEnabled(true);

                }
            }
        });
    }

    private void onOK() {
        main.Astar(textArea1, comboBox1.getSelectedIndex()+1, (radioButton1.isSelected() ? 1 : 0));
    }

    private void onCancel() {
        dispose();
    }

    public static void main(String[] args) {
        GuiDialog dialog = new GuiDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
