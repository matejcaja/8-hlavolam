import java.util.Comparator;


public class RamecManager implements Comparator<Ramec> {
    final int N = 3;
    static int[] smery = new int[4];
    public String[] translate = {"DOLE", "HORE", "DOPRAVA", "DOLAVA"};

    //metoda vynuluje smery po najdeni vsetkych nasledovnikov aktualneho Ramca
    public int[] vynulujSMery(int[] smery){
        for(int i = 0; i < 4; i++){
            smery[i] = -1;
        }

        return smery;
    }

    //metoda vytvara identifikator pre dany Ramec z pozicii policok
    public String saveArrayToString(int[][] array){
        String id = "";

        for(int i = 0; i < N; i++){
            for(int j = 0; j < N; j++){
                id += array[i][j];
            }
        }

        return id;
    }

    /*  metoda na zaklade vyberu pouzije jednu z heuristik na zistenie vzdialenosti do ciela
        na vstupe ma poziciu aktualneho Ramca a ciela + ktora heuristika ma byt pouzita
        metoda nasledne vrati ciselnu hodnotu
     */
    public int findMyPlace(int[][] uzly, int[][] result, int heuristika){
        int celkova_hodnota = 0;
        if(heuristika == 1){
            for(int i = 0; i < N; i++){
                for(int j = 0; j < N; j++){
                    for(int a = 0; a < N; a++){
                        for(int b = 0; b < N; b++){
                            if(result[a][b] == uzly[i][j]){
                                celkova_hodnota += (Math.abs(a-i)+Math.abs(b-j));
                                break;
                            }
                        }
                    }
                }
            }
        }else if(heuristika == 0){
            for(int i = 0; i < N; i++){
                for(int j = 0; j < N; j++){
                    if(result[i][j] != uzly[i][j]){
                        celkova_hodnota++;
                    }
                }
            }
        }
        return celkova_hodnota;
    }

    public int[][] copyCurrentUzly(int[][] currentRamec){
        int[][] currentUzly = new int[3][3];

        for(int i = 0; i < N; i++){
            for(int j = 0; j < N; j++){
                currentUzly[i][j] = currentRamec[i][j];
            }
        }

        return currentUzly;
    }

    // metoda najde vsetky smery, ktorymi sa da pohnut pre dany ramec
    public int[][] calculateStep(int[][] start){
        int pomocny;
        int[][] uzly = new int[3][3];

        for(int i = 0; i < N; i++){
            for(int j = 0; j < N; j++){
                uzly[i][j] = start[i][j];
            }
        }


        for(int i = 0; i < N; i++){
            for(int j = 0; j < N; j++){
                if(uzly[i][j] == 0){
                    if((i - 1) >= 0 && smery[0] == -1){//case UP
                        pomocny = uzly[i][j];

                        uzly[i][j] = uzly[i-1][j];
                        uzly[i-1][j] = pomocny;

                        smery[0] = 1;


                        return uzly;
                    }
                    if((i + 1) < N && smery[1] == -1){//case DOWN
                        pomocny = uzly[i][j];

                        uzly[i][j] = (uzly[i+1][j]);
                        uzly[i+1][j] = (pomocny);

                        smery[1] = 1;


                        return uzly;
                    }
                    if((j - 1) >= 0 && smery[2] == -1){//case LEFT
                        pomocny = uzly[i][j];

                        uzly[i][j] = (uzly[i][j-1]);
                        uzly[i][j-1] = pomocny;

                        smery[2] = 1;


                        return uzly;
                    }
                    if((j + 1) < N && smery[3] == -1){//case RIGHT
                        pomocny = uzly[i][j];

                        uzly[i][j] = (uzly[i][j+1]);
                        uzly[i][j+1] = pomocny;

                        smery[3] = 1;


                        return uzly;
                    }

                }
            }

        }
        return null;

    }

    @Override
    public int compare(Ramec ramec, Ramec ramec2) {
        if(ramec.f < ramec2.f){
            return -1;
        }else if(ramec.f > ramec2.f){
            return 1;
        }
        return 0;
    }

    public int findDirection(){
        int dir = -1;

        for(int i = 0; i < 4; i++){
            if(smery[i] == 1){
                dir = i;
                smery[i] = 2;
            }
        }

        return dir;
    }

    public String translateDirection(int dir){
        return translate[dir];
    }
}
