import javax.swing.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

public class XmlManager{
    static private File file;

    public XmlManager(){

    }

    public XmlManager(File file){
        this.file = file;
    }

    public NodeList xmlOpen(){
        NodeList nList = null;

        try{
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();
            nList = doc.getElementsByTagName("matrix");


        }catch(Exception e){
            e.getMessage();
        }


        return nList;
    }

    public int[][] xmlRead(String value){
        NodeList nList = xmlOpen();
        int[][] uzly = new int[3][3];

        for(int i = 0; i < nList.getLength(); i++){
            Node nNode = nList.item(i);

            if (nNode.getNodeType() == Node.ELEMENT_NODE && ((Element) nNode).getAttribute("name").equals(value)){
                Element e = (Element) nNode;

                uzly[0][0] = Integer.parseInt(e.getElementsByTagName("zero").item(0).getTextContent());
                uzly[0][1] = Integer.parseInt(e.getElementsByTagName("one").item(0).getTextContent());
                uzly[0][2] = Integer.parseInt(e.getElementsByTagName("two").item(0).getTextContent());
                uzly[1][0] = Integer.parseInt(e.getElementsByTagName("three").item(0).getTextContent());
                uzly[1][1] = Integer.parseInt(e.getElementsByTagName("four").item(0).getTextContent());
                uzly[1][2] = Integer.parseInt(e.getElementsByTagName("five").item(0).getTextContent());
                uzly[2][0] = Integer.parseInt(e.getElementsByTagName("six").item(0).getTextContent());
                uzly[2][1] = Integer.parseInt(e.getElementsByTagName("seven").item(0).getTextContent());
                uzly[2][2] = Integer.parseInt(e.getElementsByTagName("eight").item(0).getTextContent());

                break;
            }
        }

        return uzly;
    }

    public void xmlShow(JTextArea textArea, JComboBox comboBox){
        NodeList nList = xmlOpen();
        int size = 1;

        for(int i = 0; i < nList.getLength(); i++){
            Node nNode = nList.item(i);

            textArea.append(((Element) nNode).getAttribute("name")+"\n");
            if(((Element) nNode).getAttribute("name").equals("start_"+size)){
                size++;
            }

            if (nNode.getNodeType() == Node.ELEMENT_NODE){
                Element e = (Element) nNode;

                textArea.append(Integer.parseInt(e.getElementsByTagName("zero").item(0).getTextContent()) +" ");
                textArea.append(Integer.parseInt(e.getElementsByTagName("one").item(0).getTextContent()) + " ");
                textArea.append(Integer.parseInt(e.getElementsByTagName("two").item(0).getTextContent()) +"\n");
                textArea.append(Integer.parseInt(e.getElementsByTagName("three").item(0).getTextContent()) + " ");
                textArea.append(Integer.parseInt(e.getElementsByTagName("four").item(0).getTextContent()) + " ");
                textArea.append(Integer.parseInt(e.getElementsByTagName("five").item(0).getTextContent()) + "\n");
                textArea.append(Integer.parseInt(e.getElementsByTagName("six").item(0).getTextContent()) + " ");
                textArea.append(Integer.parseInt(e.getElementsByTagName("seven").item(0).getTextContent()) + " ");
                textArea.append(Integer.parseInt(e.getElementsByTagName("eight").item(0).getTextContent())+"\n\n");
            }
        }

        comboBox.removeAllItems();
        for(int i = 1; i < size; i++){
            comboBox.addItem(i+"");
        }
    }
}